/*******************************************************************************
 * Copyright (C) 2019 nicmus inc. (jivko.sabev@gmail.com)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package com.nicmus.jpoweradmin.service;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.SetJoin;

import org.slf4j.Logger;

import com.nicmus.jpoweradmin.model.orm.Domain;
import com.nicmus.jpoweradmin.model.orm.Domain_;
import com.nicmus.jpoweradmin.model.orm.Role;
import com.nicmus.jpoweradmin.model.orm.User;
import com.nicmus.jpoweradmin.model.orm.User_;
import com.nicmus.jpoweradmin.utils.JPowerAdmin;

@Singleton
@ApplicationScoped
public class AccountPurgerService implements Serializable {
	private static final int DAYS = -30;
	private static final long serialVersionUID = -1291359393123602921L;
	@Inject @JPowerAdmin
	private EntityManager entityManager;
	@Inject
	private Logger logger;
	
	/**
	 * Run every day at midnight
	 * Purge deleted accounts that are more than 30 days old
	 */
	@Schedule(hour="0", dayOfMonth="*")
	public void purgeDeletedAccounts() {
		//delete all accounts marked for deletion
		CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, DAYS);
		Date thirtyDaysAgo = cal.getTime();
		
		//Delete domains belonging to deleted accounts first
		CriteriaQuery<Domain> domainCriteria = criteriaBuilder.createQuery(Domain.class);
		Root<Domain> domainFrom = domainCriteria.from(Domain.class);
		SetJoin<Domain, User> userJoin = domainFrom.join(Domain_.users);
		Predicate isDeletedPredicate = criteriaBuilder.isTrue(userJoin.get(User_.deleted));
		Predicate thirtyDaysPredicate = criteriaBuilder.lessThan(userJoin.get(User_.dateModified), thirtyDaysAgo);
		
		CriteriaQuery<Domain> criteriaQuery = domainCriteria.select(domainFrom).where(criteriaBuilder.and(isDeletedPredicate,thirtyDaysPredicate));
		List<Domain> domainsToDelete = this.entityManager.createQuery(criteriaQuery).getResultList();
		this.logger.info("Domains belonging to deleted accounts: {}", domainsToDelete.size());
		for(Domain d : domainsToDelete) {
			Set<User> users = d.getUsers();
			for(User u : users) {
				u.getDomains().remove(d);
			}
			this.entityManager.remove(d);
		}
		this.logger.info("Finished deleting domains");

		//delete users 
		CriteriaQuery<User> userCriteriaQuery = criteriaBuilder.createQuery(User.class);
		Root<User> userFrom = userCriteriaQuery.from(User.class);
		Predicate deleteUserPredicate = criteriaBuilder.isTrue(userFrom.get(User_.deleted));
		Predicate lessThan = criteriaBuilder.lessThan(userFrom.get(User_.dateModified), thirtyDaysAgo);
		
		Predicate whereCondition = criteriaBuilder.and(deleteUserPredicate,lessThan);
		
		List<User> usersToDelete = this.entityManager.createQuery(userCriteriaQuery.select(userFrom).where(whereCondition)).getResultList();
		this.logger.info("Loaded {} deleted users for purging", usersToDelete.size());
		for(User u : usersToDelete) {
			Set<Role> roles = u.getRoles();
			for(Role r : roles) {
				this.entityManager.remove(r);
			}
			this.entityManager.remove(u);	
		}
		this.logger.info("Finished purging account data");
		
		
	}

}
