/*******************************************************************************
 * Copyright (C) 2017 nicmus inc. (jivko.sabev@gmail.com)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.nicmus.jpoweradmin.service;

import java.io.Serializable;
import java.net.IDN;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import com.nicmus.jpoweradmin.model.orm.Record;
import com.nicmus.jpoweradmin.model.orm.Record.TYPE;

public class DNSValidator implements Serializable {
	private static final long serialVersionUID = -8162972050313878159L;


	//This monstrosity came from:
	//http://www.schlitt.net/spf/tests/spf_record_regexp-03.txt
	private static final Pattern SPF_PATTERN = Pattern.compile(
			"^[Vv]=[Ss][Pp][Ff]1( +([-+?~]?([Aa][Ll][Ll]|[Ii][Nn][Cc][Ll][Uu][Dd]" +
			"[Ee]:(%\\{[CDHILOPR-Tcdhilopr-t]([1-9][0-9]?|10[0-9]|11[0-9]|12[0-8])?[Rr]?" +
			"[+-/=_]*\\}|%%|%_|%-|[!-$&-~])*(\\.([A-Za-z]|[A-Za-z]([-0-9A-Za-z]?)*" +
			"[0-9A-Za-z])|%\\{[CDHILOPR-Tcdhilopr-t]([1-9][0-9]?|10[0-9]|11[0-9]|" +
			"12[0-8])?[Rr]?[+-/=_]*\\})|[Aa](:(%\\{[CDHILOPR-Tcdhilopr-t]([1-9][0-9]" +
			"?|10[0-9]|11[0-9]|12[0-8])?[Rr]?[+-/=_]*\\}|%%|%_|%-|[!-$&-~])*" +
			"(\\.([A-Za-z]|[A-Za-z]([-0-9A-Za-z]?)*[0-9A-Za-z])|%\\{[CDHILOPR-Tcdhilopr-t]" +
			"([1-9][0-9]?|10[0-9]|11[0-9]|12[0-8])?[Rr]?[+-/=_]*\\}))?" +
			"((/([1-9]|1[0-9]|2[0-9]|3[0-2]))?(//([1-9][0-9]?|10[0-9]|11[0-9]|12[0-8]))?)?" +
			"|[Mm][Xx](:(%\\{[CDHILOPR-Tcdhilopr-t]([1-9][0-9]?|10[0-9]|11[0-9]|12[0-8])?[Rr]?" +
			"[+-/=_]*\\}|%%|%_|%-|[!-$&-~])*(\\.([A-Za-z]|[A-Za-z]([-0-9A-Za-z]?)*[0-9A-Za-z])|%" +
			"\\{[CDHILOPR-Tcdhilopr-t]([1-9][0-9]?|10[0-9]|11[0-9]|12[0-8])?[Rr]?[+-/=_]*\\}))?" +
			"((/([1-9]|1[0-9]|2[0-9]|3[0-2]))?(//([1-9][0-9]?|10[0-9]|11[0-9]|12[0-8]))?)?|[Pp]" +
			"[Tt][Rr](:(%\\{[CDHILOPR-Tcdhilopr-t]([1-9][0-9]?|10[0-9]|11[0-9]|12[0-8])?[Rr]?" +
			"[+-/=_]*\\}|%%|%_|%-|[!-$&-~])*(\\.([A-Za-z]|[A-Za-z]([-0-9A-Za-z]?)*[0-9A-Za-z])|" +
			"%\\{[CDHILOPR-Tcdhilopr-t]([1-9][0-9]?|10[0-9]|11[0-9]|12[0-8])?[Rr]?[+-/=_]*\\}))" +
			"?|[Ii][Pp]4:([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|" +
			"1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])" +
			"\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(/([1-9]|1[0-9]|2[0-9]|3[0-2]))" +
			"?|[Ii][Pp]6:(::|([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4}|([0-9A-Fa-f]{1,4}:){1,8}:|" +
			"([0-9A-Fa-f]{1,4}:){7}:[0-9A-Fa-f]{1,4}|([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}){1,2}|" +
			"([0-9A-Fa-f]{1,4}:){5}(:[0-9A-Fa-f]{1,4}){1,3}|([0-9A-Fa-f]{1,4}:){4}(:[0-9A-Fa-f]{1,4})" +
			"{1,4}|([0-9A-Fa-f]{1,4}:){3}(:[0-9A-Fa-f]{1,4}){1,5}|([0-9A-Fa-f]{1,4}:){2}(:[0-9A-Fa-f]" +
			"{1,4}){1,6}|[0-9A-Fa-f]{1,4}:(:[0-9A-Fa-f]{1,4}){1,7}|:(:[0-9A-Fa-f]{1,4}){1,8}|([0-9A-Fa-f]" +
			"{1,4}:){6}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4]" +
			"[0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}" +
			"|2[0-4][0-9]|25[0-5])|([0-9A-Fa-f]{1,4}:){6}:([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\." +
			"([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])" +
			"\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])|([0-9A-Fa-f]{1,4}:){5}:([0-9A-Fa-f]{1,4}:)?" +
			"([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\." +
			"([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])|" +
			"([0-9A-Fa-f]{1,4}:){4}:([0-9A-Fa-f]{1,4}:){0,2}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\." +
			"([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\." +
			"([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])|([0-9A-Fa-f]{1,4}:){3}:([0-9A-Fa-f]{1,4}:){0,3}([0-9]|" +
			"[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\." +
			"([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])|" +
			"([0-9A-Fa-f]{1,4}:){2}:([0-9A-Fa-f]{1,4}:){0,4}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\." +
			"([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\." +
			"([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])|[0-9A-Fa-f]{1,4}::([0-9A-Fa-f]{1,4}:)" +
			"{0,5}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])" +
			"\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\." +
			"([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\." +
			"([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])|::" +
			"([0-9A-Fa-f]{1,4}:){0,6}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\." +
			"([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\." +
			"([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\." +
			"([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))" +
			"(/([1-9][0-9]?|10[0-9]|11[0-9]|12[0-8]))?|" +
			"[Ee][Xx][Ii][Ss][Tt][Ss]:(%\\{[CDHILOPR-Tcdhilopr-t]" +
			"([1-9][0-9]?|10[0-9]|11[0-9]|12[0-8])?[Rr]?[+-/=_]*\\}|%%|%_|%-|[!-$&-~])*" +
			"(\\.([A-Za-z]|[A-Za-z]([-0-9A-Za-z]?)*[0-9A-Za-z])|%\\{[CDHILOPR-Tcdhilopr-t]" +
			"([1-9][0-9]?|10[0-9]|11[0-9]|12[0-8])?[Rr]?[+-/=_]*\\}))|[Rr][Ee][Dd][Ii][Rr][Ee][Cc][Tt]=" +
			"(%\\{[CDHILOPR-Tcdhilopr-t]([1-9][0-9]?|10[0-9]|11[0-9]|12[0-8])?[Rr]?[+-/=_]*\\}|%%|%_|%-|[!-$&-~])*" +
			"(\\.([A-Za-z]|[A-Za-z]([-0-9A-Za-z]?)*[0-9A-Za-z])|" +
			"%\\{[CDHILOPR-Tcdhilopr-t]([1-9][0-9]?|10[0-9]|11[0-9]|12[0-8])?[Rr]?[+-/=_]*\\})" +
			"|[Ee][Xx][Pp]=(%\\{[CDHILOPR-Tcdhilopr-t]([1-9][0-9]?|10[0-9]" +
			"|11[0-9]|12[0-8])?[Rr]?[+-/=_]*\\}|%%|%_|%-|[!-$&-~])*(\\.([A-Za-z]|[A-Za-z]" +
			"([-0-9A-Za-z]?)*[0-9A-Za-z])|%\\{[CDHILOPR-Tcdhilopr-t]" +
			"([1-9][0-9]?|10[0-9]|11[0-9]|12[0-8])?[Rr]?[+-/=_]*\\})|[A-Za-z]" +
			"[-.0-9A-Z_a-z]*=(%\\{[CDHILOPR-Tcdhilopr-t]([1-9][0-9]?|10[0-9]|" +
			"11[0-9]|12[0-8])?[Rr]?[+-/=_]*\\}|%%|%_|%-|[!-$&-~])*))* *$");


	
	@Inject
	private RecordServiceDAO recordServiceDAO;
	
	
	/**
	 * Validate that the given record is valid within the list of records in the 
	 * zone
	 * @param record The record to validate
	 * @return {@link ValidationResult}
	 */
	public ValidationResult validateRecord(Record record){
		ValidationResult validationResult = new ValidationResult();
		//see if the record exists
		String asciiName = IDN.toASCII(record.getName());
		String asciiContent = IDN.toASCII(record.getContent());
		if(this.recordServiceDAO.exists(record.getName(), record.getContent(), record.getType())){
			FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Record already exists", "");
			validationResult.getFacesMessages().add(fc);
			return validationResult;			
		}

		
		ValidationResult validFQDN = this.validateFQDN(asciiName);
		ValidationResult validFQDNAscii = this.validateFQDN(asciiContent);
		switch(record.getType()){
			case A:{
				if(!validFQDN.isValidated()) {
					return validFQDN;
				}
				ValidationResult validIPV4 = this.validateIPV4(record.getContent());
				if(!validIPV4.isValidated()) {
					return validIPV4;
				}
				if(this.recordServiceDAO.exists(record.getName(), TYPE.CNAME)){
					FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR, "There already exists a CNAME matching " + record.getName(),"");
					validationResult.getFacesMessages().add(fc);
				}
				return validationResult;
			}
			case AAAA: {
				if(!validFQDN.isValidated()) {
					return validFQDN;
				}
				ValidationResult validIPV6 = this.validateIPV6(record.getContent());
				if(!validIPV6.isValidated()) {
					return validIPV6;
				}
				if(this.recordServiceDAO.exists(record.getName(), TYPE.CNAME)){
					FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR, "There already exists a CNAME matching " + record.getName(),"");
					validationResult.getFacesMessages().add(fc);				
				}
				return validationResult;
			}
			case CNAME: {
				if(!validFQDN.isValidated()) {
					return validFQDN;
				}
				if(!validFQDNAscii.isValidated()) {
					return validFQDNAscii;
				}
				if(this.recordServiceDAO.contentExists(record.getName(), TYPE.NS)) {
					FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Not a valid CNAME. Check for MX or NS record", "");
					validationResult.getFacesMessages().add(fc);
				}
				
				if(this.recordServiceDAO.contentExists(record.getName(), TYPE.MX)){
					FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Not a valid CNAME. Check for MX or NS record", "");
					validationResult.getFacesMessages().add(fc);
				}

				if(this.recordServiceDAO.exists(record.getName(), TYPE.A)){
					FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR, "This is not a valid CNAME. There is already exists an A, AAAA or CNAME with such a name", "");
					validationResult.getFacesMessages().add(fc);
				}
				if(this.recordServiceDAO.exists(record.getName(), TYPE.AAAA)){
					FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR, "This is not a valid CNAME. There is already exists an A, AAAA or CNAME with such a name", "");
					validationResult.getFacesMessages().add(fc);
				}

				if(this.recordServiceDAO.exists(record.getName(), TYPE.CNAME)){
					FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR, "This is not a valid CNAME. There is already exists an A, AAAA or CNAME with such a name", "");
					validationResult.getFacesMessages().add(fc);
				}

				return validationResult;
			}
			case MX:{
				if(!validFQDN.isValidated()) {
					return validFQDN;
				}
				if(!validFQDNAscii.isValidated()) {
					return validFQDNAscii;
				}
				if(this.recordServiceDAO.contentExists(record.getContent(), TYPE.CNAME)){
					FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR, "There already exists a CNAME matching " + record.getName(),"");
					validationResult.getFacesMessages().add(fc);				
				}
				return validationResult;
			}
			case NS: {
				if(!validFQDN.isValidated()) {
					return validFQDN;
				}
				if(!validFQDNAscii.isValidated()) {
					return validFQDNAscii;
				}
				if(this.recordServiceDAO.contentExists(record.getContent(), TYPE.CNAME)){
					FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR, "There already exists a CNAME matching " + record.getName(),"");
					validationResult.getFacesMessages().add(fc);
					
				}
				return validationResult;
			}
			case PTR: {
				if(!validFQDN.isValidated()) {
					return validFQDN;
				}
				if(!validFQDNAscii.isValidated()) {
					return validFQDNAscii;
				}
				return validationResult;
			}
			case TXT: {
				if(!this.validatePrintable(record.getName()).isValidated()) {
					return this.validatePrintable(record.getName());
				}
				if(!this.validatePrintable(record.getContent()).isValidated()) {
					return this.validatePrintable(record.getContent());
				}
				record.setName(this.quoteRecordContent(record.getName()));
				record.setContent(this.quoteRecordContent(record.getContent()));
				return validationResult;
			}
			case SPF: {
				if(!this.validateSPF(asciiContent).isValidated()) {
					return this.validateSPF(asciiContent);
				}
				record.setName(this.quoteRecordContent(record.getName()));
				record.setContent(this.quoteRecordContent(record.getContent()));
				return validationResult;
			}
			default:
				return validationResult;
		}
	}

	/**
	 * 
	 * @param hostname
	 * @return
	 */
	public ValidationResult validateFQDN(String hostname){
		ValidationResult validationResult = new ValidationResult();
		if(hostname.length() > 255){
			FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Hostname too long - maximum length allowed is 255 characters","");
			validationResult.getFacesMessages().add(fc);
			return  validationResult;
		}
		if(hostname.indexOf('.') == -1){
			FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Invalid hostname","");
			validationResult.getFacesMessages().add(fc);
			return validationResult;
			
		}
		String[] parts= hostname.split("\\.");
		int position = 0;
		for(String part : parts){
			if(position++ == 0){
				//we are at the first hostname part
				//could start with '*'
				if(!part.matches("^(\\*|[\\w-]+)$")){
					FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Invalid part","");
					validationResult.getFacesMessages().add(fc);
					return validationResult;
				}
			} else {
				//no '*' is allowed
				if(!part.matches("[\\w-]+$")){
					FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Invalid character " + part,"");
					validationResult.getFacesMessages().add(fc);
					return validationResult;
				}
			}
			//starts or ends in '-';
			if(part.startsWith("-") || part.endsWith("-")){
				FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Invalid character " + part,"");
				validationResult.getFacesMessages().add(fc);
				return validationResult;
			}
			if(part.length() < 1){
				FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid length","");
				validationResult.getFacesMessages().add(facesMessage);
				return  validationResult;
			}
			
			if(part.length() > 63){
				FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Hostname part too big " + part,"");
				validationResult.getFacesMessages().add(fc);
				return validationResult;
			}
		}
		return validationResult;
		
	}
	
	/**
	 * Validate IPV4
	 * @param ip the ip address
	 * @return {@link ValidationResult}
	 */
	public ValidationResult validateIPV4(String ip){
		ValidationResult validationResult = new ValidationResult();
		
		if(!ip.matches("^[0-9\\.]{7,15}$")){
			FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Invalid format","");
			validationResult.getFacesMessages().add(fc);
		}
		
		String[] ipQuads = ip.split("\\.");
		if(ipQuads != null && ipQuads.length !=4){
			FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Invalid IP part","");
			validationResult.getFacesMessages().add(fc);
		}
		
		if(ipQuads != null){
			for(String quad : ipQuads){
				try {
					int parseInt = Integer.parseInt(quad);
					if(parseInt > 255){
						FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Invalid IP quad " + quad,"");
						validationResult.getFacesMessages().add(fc);
					}
				} catch (NumberFormatException e){
					FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Invalid IP character","");
					validationResult.getFacesMessages().add(fc);
				}
			}
		} else {
			FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Invalid IP","");
			validationResult.getFacesMessages().add(fc);
		}
		
		return validationResult;
	}
	

	/**
	 * 
	 * @param ip
	 * @return {@link ValidationResult}
	 */
	public ValidationResult validateIPV6(String ip){
		ValidationResult validationResult = new ValidationResult();
		if(!ip.matches("(?i)^[0-9a-f]{0,4}:([0-9a-f]{0,4}:){0,6}[0-9a-f]{0,4}$")){
			FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Invalid IPv6 address","");
			validationResult.getFacesMessages().add(fc);
		}
		
		String[] hexParts = ip.split(":");
		if(hexParts == null){
			FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Invalid IPv6 address","");
			validationResult.getFacesMessages().add(fc);
		}
		
		if(hexParts.length > 8 || hexParts.length < 3){
			FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Invalid hex part","");
			validationResult.getFacesMessages().add(fc);
		}
		
		int emptyHexParts = 0;
		for(String hexPart : hexParts){
			if(hexPart.isEmpty()){
				emptyHexParts++;
			}
		}
		
		if(emptyHexParts == 0 && hexParts.length != 8){
			FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Invalid IPv6 format","");
			validationResult.getFacesMessages().add(fc);
		}
		
		return validationResult;
	}
		
	/**
	 * 
	 * @param string
	 * @return {@link ValidationResult}
	 */
	public ValidationResult validatePrintable(String string){
		boolean pritable = string.matches("^[\\p{Print}]+$");
		ValidationResult vr = new ValidationResult();
		if(!pritable){
			FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Non printable characters found","");
			vr.getFacesMessages().add(fc);
		}
		return vr;
	}

	/**
	 * 
	 * @param content
	 * @return {@link ValidationResult}
	 */
	public ValidationResult validateSPF(String content){
		ValidationResult validationResult = new ValidationResult();
		if(SPF_PATTERN.matcher(content).matches()){
			return validationResult;
		}
		FacesMessage fc = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Invalid SRV target","");
		validationResult.getFacesMessages().add(fc);
		return validationResult;
	}

	/**
	 * Enclose the record content in quotation marks if there are spaces found. 
	 * Applicable for TXT and SRV records
	 * @param text 
	 * @return 
	 */
	private String quoteRecordContent(String text){
		if(text != null){
			if(text.split("\\s+").length > 1 && !(text.startsWith("\"") && text.endsWith("\""))){
				text = "\"" + text + "\"";
			}
		}
		return text;
	}

	/**
	 * Validateion result. Wraps a list of FacesMessages containing the 
	 * validation messages in cases where validation fails. Successful 
	 * validation is indicated by an empty facesMessages.
	 * @author jsabev
	 *
	 */
	public class ValidationResult {
		private List<FacesMessage> facesMessages = new ArrayList<>();
		
		
		/**
		 * @return the validated
		 */
		public boolean isValidated() {
			return this.facesMessages.isEmpty();
		}

		/**
		 * @return the facesMessages
		 */
		public List<FacesMessage> getFacesMessages() {
			return this.facesMessages;
		}
		
		
		
	}
	
}
