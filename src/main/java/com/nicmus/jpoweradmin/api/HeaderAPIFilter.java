/*******************************************************************************
 * Copyright (C) 2019 nicmus inc. (jivko.sabev@gmail.com)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.nicmus.jpoweradmin.api;

import java.io.IOException;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import com.nicmus.jpoweradmin.model.orm.User;

/**
 * Security filter to check that the header is set correctly and the user exists
 * @author jsabev
 *
 */
@Provider
public class HeaderAPIFilter implements ContainerRequestFilter {

	protected static final String API_HEADER_NAME = "X-JPowerAdmin-API-Key";
	
	@Inject
	private UserServiceDAO userServiceDAO;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		String apiKey = requestContext.getHeaderString(API_HEADER_NAME);
		if(apiKey == null || apiKey.trim().isEmpty()) {
			ResponseBuilder builder = Response.status(Status.FORBIDDEN);
			builder.entity("Missing request header: " +  API_HEADER_NAME);
			requestContext.abortWith(builder.build());
			return;
		}
		
		try {
			@SuppressWarnings("unused")
			User userByApi = this.userServiceDAO.getUserByApi(apiKey);
		} catch (EJBException e) {
			ResponseBuilder responseBuilder = Response.status(Status.NOT_FOUND);
			responseBuilder.entity(e.getLocalizedMessage());
			requestContext.abortWith(responseBuilder.build());
		}
	}

}
