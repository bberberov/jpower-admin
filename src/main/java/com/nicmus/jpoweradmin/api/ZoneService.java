/*******************************************************************************
 * Copyright (C) 2019 nicmus inc. (jivko.sabev@gmail.com)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.nicmus.jpoweradmin.api;


import java.net.IDN;
import java.util.List;

import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;

import com.nicmus.jpoweradmin.model.orm.Domain;
import com.nicmus.jpoweradmin.model.orm.Record;
import com.nicmus.jpoweradmin.model.orm.User;
import com.nicmus.jpoweradmin.service.DNSValidator;
import com.nicmus.jpoweradmin.service.DNSValidator.ValidationResult;
import com.nicmus.jpoweradmin.service.RecordServiceDAO;
import com.nicmus.jpoweradmin.service.ZoneServiceDAO;

@Path("/zone")
public class ZoneService {
	
	@HeaderParam(com.nicmus.jpoweradmin.api.HeaderAPIFilter.API_HEADER_NAME)
	private String apiKey;
	
	@Inject
	private Logger logger;
	
	@Inject
	private com.nicmus.jpoweradmin.service.ZoneService zoneService;
	
	@Inject
	private DNSValidator dnsValidator;
	
	@Inject
	private ZoneServiceDAO zoneServiceDAO;
	
	@Inject
	private RecordServiceDAO recordServiceDAO;
	
	@Inject
	private UserService userService;
	
	/**
	 * Return the number of zones for the given api key
	 * @return the number of zones
	 */
	@Path("/count")
	@GET
	@Produces({MediaType.TEXT_PLAIN})
	public long getNumZones() {
		User user = this.userService.getUser();
		return this.zoneServiceDAO.getZoneCount(user.getUserName());
		
	}

	/**
	 * Return the number of zones for the given user. Only available to ROOT 
	 * USERS
	 * @param userName username whose zone count to get
	 * @return the number of zones for the given user
	 */
	@Path("/{userName}/count")
	@GET
	@Produces({MediaType.TEXT_PLAIN})
	public long getNumZones(@PathParam("userName") String userName) {
		User user = this.userService.getUser();
		if(user.isRoot()) {
			return this.zoneServiceDAO.getZoneCount(userName);
		} else {
			throw new WebApplicationException(Response.status(Status.UNAUTHORIZED).build());		
		}
	}
	/**
	 * Get the zones listing for current user. The zone listing can be filtered by 
	 * providing the offset and max path parameters. 
	 * @param offset. Starting offset for zone list
	 * @param max. The max number of items to return (hard limited to 50)
	 * @return the zones matching the criteria
	 */
	@GET
	@Path("/list")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public List<Domain> getZones(@QueryParam("offset") int offset, @QueryParam("count") int max){
		if(offset < 0) {
			offset = 0;
		}
		final int maxResults = 50;
		if(max > maxResults || max  <= 0) {
			max = maxResults;
		}
		User user = this.userService.getUser();
		return this.zoneServiceDAO.getZones(user.getUserName(), offset, max);
	}
	
	/**
	 * Get the zones listing for the given user. Only available to ROOT accounts 
	 * @param userName the username whose zones to review
	 * @param offset the starting offset
	 * @param max the maximum number of records to return
	 * @return the list of zones matching the given criteria
	 */
	@GET
	@Path("/{userName}/list")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public List<Domain> getZones(@PathParam("userName") String userName, @QueryParam("offset") int offset, @QueryParam("count") int max){
		User user = this.userService.getUser();
		if(!user.isRoot()) {
			throw new WebApplicationException(Response.status(Status.UNAUTHORIZED).build());
		}
		if(offset < 0) {
			offset = 0;
		}
		final int maxResults = 100;
		if(max > maxResults || max  <= 0) {
			max = maxResults;
		}
		return this.zoneServiceDAO.getZones(userName, offset, max);		
	}
	
	
	/**
	 * Add the given domain to the given userName
	 * @param userName
	 * @param domain
	 */
	@POST
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public void addZone(@QueryParam("userName") String userName, Domain domain) {
		String domainName = domain.getName();
		//security check on privileges of calling user
		User callingUser = this.userService.getUser();
		if(userName == null) {
			userName = callingUser.getUserName();
		}
		if(!callingUser.getUserName().equals(userName) && !callingUser.isRoot()) {
			//cannot add a zone to a different user unless it is called by root
			throw new WebApplicationException(Response.status(Status.UNAUTHORIZED).build());
		}
		if(!this.dnsValidator.validateFQDN(IDN.toASCII(domainName)).isValidated()){
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).build());
		}
		
		this.zoneService.addInitialZoneRecords(domain);
		try {
			this.zoneServiceDAO.createZone(userName, domain);
		} catch (Exception e) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build());
		}
	}
	
	/**
	 * Delete the given zone
	 * @param zoneName
	 */
	@DELETE
	@Path("/{zoneName}")
	public void deleteZone(@PathParam("zoneName") String zoneName) {
		User user = this.userService.getUser();
		try {
			Domain zone = this.zoneServiceDAO.getZone(user.getUserName(), zoneName);		
			this.zoneServiceDAO.deleteZone(zone); 
		} catch (EJBException e) {
			this.logger.error("", e);
			throw new WebApplicationException(Response.status(Status.NOT_FOUND).build());
		}
	}
	
	//---------------------------Zone Records-----------------------------------
	
	/**
	 * Get the number of zone records for the given zone
         * @param zoneName
	 * @return the number of records for the given zone
	 */
	@GET
	@Path("/{zoneName}/records/count")
	@Produces({MediaType.TEXT_PLAIN})
	public long getZoneRecordsCount(@PathParam("zoneName") String zoneName) {
		User user = this.userService.getUser();
		return this.recordServiceDAO.getRecordCount(zoneName, user.getUserName());		
	}
	
	/**
	 * Get zone records for the given zone name
         * @param zoneName
	 * @param offset where to start - default 0
	 * @param count the maximum number of records to return - default 100. Hard limit: 100
	 * @return the zone records for the given zone
	 */
	@GET
	@Path("/{zoneName}/records")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public List<Record> getZoneRecords(@PathParam("zoneName") String zoneName, @QueryParam("offset") int offset, @QueryParam("count") int count){
		if(offset < 0) {
			offset = 0;
		}
		final int maxResults = 100;
		if(count > maxResults || count  <= 0) {
			count = maxResults;
		}
		return this.recordServiceDAO.getRecords(this.userService.getUser().getUserName(), zoneName, offset, count);
	}

	/**
	 * Get the zone record matching the parameters
	 * @param zoneName zoneName
	 * @param id recordId
	 * @return the Zone Record corresponding to id
	 */
	@GET
	@Path("/{zoneName}/record/{id}")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Record getZoneRecord(@PathParam("zoneName") String zoneName, @PathParam("id") int id) {
		User user = this.userService.getUser();
		return this.recordServiceDAO.getRecord(user.getUserName(), zoneName, id);
	}
	
	/**
	 * Create a zone record
	 * @param zoneName the zone name where the record is to be created
	 * @param record Record object to add
	 * @return the id of the record created
	 */
	@POST
	@Path("/{zoneName}")
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Produces({MediaType.TEXT_PLAIN})
	public long createZoneRecord(@PathParam("zoneName") String zoneName, Record record) {
		ValidationResult validationResult = this.dnsValidator.validateRecord(record);
		if(!validationResult.isValidated()) {
			List<FacesMessage> facesMessages = validationResult.getFacesMessages();
			StringBuilder response = new StringBuilder();
			facesMessages.forEach(f -> response.append(f.getSummary()).append(" "));
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity(response.toString()).build());
		}				
		
		User user = this.userService.getUser();
		//get the zone to which to add the record
		Domain domain = this.zoneServiceDAO.getZone(user.getUserName(), zoneName);
		
		//some extra sanity checking
		if(record.getName() == null || record.getName().isEmpty()) {
			record.setName(domain.getName());
		}
		if(!record.getName().toLowerCase().endsWith(domain.getName().toLowerCase())) {
			record.setName(record.getName().concat(".").concat(domain.getName()));
		}
		
		Record newRecord = new Record();
		newRecord.setName(record.getName());
		newRecord.setContent(record.getContent());
		newRecord.setPrio(record.getPrio());
		newRecord.setType(record.getType());
			
		newRecord.setDomain(domain);
		return this.recordServiceDAO.addRecord(newRecord);
	}
	
	/**
	 * Delete the given zone record from the given domain
	 * @param zoneName the domain the record belongs to
	 * @param id the id of the record to delete
	 */
	@DELETE
	@Path("/{zoneName}/record/{id}")
	public void deleteZoneRecord(@PathParam("zoneName") String zoneName, @PathParam("id") int id) {
		Record zoneRecord = this.getZoneRecord(zoneName, id);
		this.recordServiceDAO.deleteRecord(zoneRecord);
	}
}
